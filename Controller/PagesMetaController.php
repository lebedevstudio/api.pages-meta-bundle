<?php

declare(strict_types=1);

namespace lst\PagesMetaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PagesMetaController extends AbstractController
{
    private $serializer;

    public function __construct(NormalizerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/page-meta/{id}", name="page-meta", methods={"GET"})
     * @param int $id
     * @return JsonResponse
     */
    public function getPageMeta(int $id) : JsonResponse
    {
        return new JsonResponse([
            'status'  => 200,
            'message' => 'OK',
        ]);
    }
}
