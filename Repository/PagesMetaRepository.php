<?php

namespace lst\PagesMetaBundle\Repository;

use lst\PagesMetaBundle\Entity\PageMeta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PageMeta|null find($id, $lockMode = null, $lockVersion = null)
 * @method PageMeta|null findOneBy(array $criteria, array $orderBy = null)
 * @method PageMeta[]    findAll()
 * @method PageMeta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PagesMetaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PageMeta::class);
    }
}
