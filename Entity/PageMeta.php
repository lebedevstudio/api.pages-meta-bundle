<?php

declare(strict_types=1);

namespace lst\PagesMetaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clients Test Entity
 * @ORM\Table(name="pages_meta")
 * @ORM\Entity(repositoryClass="lst\PagesMetaBundle\Repository\PagesMetaRepository")
 */
class PageMeta
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $alias;
    /**
     * @var string
     * @ORM\Column(type="string", length=60, nullable=true)
     */

    protected $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $keywords;

    /**
     * @var string
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $description;
}
